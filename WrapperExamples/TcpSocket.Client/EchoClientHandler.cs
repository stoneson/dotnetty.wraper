﻿// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

namespace TcpSocket.Client
{
    using System;
    using System.Text;
    using CNative.Logging;
    using DotNetty.Buffers;
    using DotNetty.Transport.Channels;
    using DotNetty.Wraper;
    using Examples.Common;
    using Microsoft.Extensions.Logging;

    public class EchoClientHandler : BaseSimpleChannelInboundHandler<object>
    {
        readonly IByteBuffer initialMessage; 
        private readonly ILogger _logger = new ConfigLogger();//LoggingHelper.Logger;

        public EchoClientHandler(IChannelEvent channelEvent) : base(channelEvent)
        {
            this.initialMessage = Unpooled.Buffer(ClientSettings.Size);
            byte[] messageBytes = Encoding.UTF8.GetBytes("Hello world");
            this.initialMessage.WriteBytes(messageBytes);
        }

        public override void ChannelActive(IChannelHandlerContext context)
        {
            base.ChannelActive(context);
            context.WriteAndFlushAsync(this.initialMessage);
        }

        public override void ChannelRead(IChannelHandlerContext context, object message)
        {
            var byteBuffer = message as IByteBuffer;
            if (byteBuffer != null)
            {
                //Console.WriteLine("Received from server: " + byteBuffer.ToString(Encoding.UTF8));
                _logger.LogInformation("Received from server: " + byteBuffer.ToString(Encoding.UTF8));
            }
            context.WriteAsync(message);
        }

    }
}