﻿
using DotNetty.Wraper;
using Examples.Common;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace WebSocket.ConsoleClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();// LoggingHelper.Logger;
            var option = new ClientOption();
            option.SeverIp = Examples.Common.ClientSettings.Host.ToString();
            option.Port = Examples.Common.ClientSettings.Port;
            option.Path = Examples.Common.ClientSettings.Path;
            option.UseLibuv = Examples.Common.ClientSettings.UseLibuv;
            if (Examples.Common.ServerSettings.IsSsl)
            {
                option.Certificate = System.IO.Path.Combine(ExampleHelper.ProcessDirectory, "dotnetty.com.pfx");
                option.CertificatePassword = "password";
            }
            var theClient = await SocketBuilderFactory.GetWebSocketClientBuilder(option)
                .OnClientStarted(client =>
                {
                    _logger.LogInformation($"客户端启动");
                })
                .OnClientClose(client =>
                {
                    _logger.LogInformation($"客户端关闭");
                })
                .OnException(ex =>
                {
                    _logger.LogError($"异常:{ex.Message}");
                })
                .OnRecieve((client, msg) =>
                {
                    _logger.LogInformation($"客户端:收到数据:{msg}");
                    client.Send("Hello world" + DateTime.Now);
                })
                .OnSend((client, msg) =>
                {
                    _logger.LogDebug($"客户端:发送数据:{msg}");
                })
                .OnChannelRegistered((client, channel) =>
                {
                    //client.Send("Hello world" + DateTime.Now);
                    _logger.LogDebug("OnChannelRegistered: " + channel.Id);
                })
                .OnChannelUnregistered((client, channel) =>
                {
                    _logger.LogDebug("OnChannelUnregistered: " + channel.Id);
                })
                .BuildAsync((pipeline) =>
                {
                });

            //while (true)
            //{
            //    await theClient.Send(Guid.NewGuid().ToString());

            //    await Task.Delay(1000);
            //}
            Console.ReadLine();
        }
    }
}