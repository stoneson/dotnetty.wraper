﻿using System.Net;

namespace DotNetty.Wraper
{
    /// <summary>
    /// Socket构建者工厂
    /// </summary>
    public class SocketBuilderFactory
    {
        static SocketBuilderFactory()
        {
            //System.Environment.SetEnvironmentVariable("io.netty.allocator.numDirectArenas", "0");
            System.Environment.SetEnvironmentVariable("io.netty.allocator.numHeapArenas", "0");
        }

        /// <summary>
        /// 获取TcpSocket客户端构建者
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static ITcpSocketClientBuilder GetTcpSocketClientBuilder(ClientOption option)
        {
            return new TcpSocketClientBuilder(option);
        }
        /// <summary>
        /// 获取TcpSocket客户端构建者
        /// </summary>
        /// <param name="ip">服务器Ip</param>
        /// <param name="port">服务器端口</param>
        /// <returns></returns>
        public static ITcpSocketClientBuilder GetTcpSocketClientBuilder(string ip, int port)
        {
            var option = new ClientOption() { SeverIp = ip, Port = port };
            return GetTcpSocketClientBuilder(option);
        }
        /// <summary>
        /// 获取TcpSocket客户端构建者
        /// </summary>
        /// <param name="remoteAddress">服务器Ip 服务器端口</param>
        /// <returns></returns>
        public static ITcpSocketClientBuilder GetTcpSocketClientBuilder(EndPoint remoteAddress)
        {
            var eip = remoteAddress as System.Net.IPEndPoint;
            return GetTcpSocketClientBuilder(eip.Address.ToString(), eip.Port);
        }
        //------------------------------------------------------------------------------------------------
        /// <summary>
        /// 获取TcpSocket服务端构建者
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static ITcpSocketServerBuilder GetTcpSocketServerBuilder(ServerOption option)
        {
            return new TcpSocketServerBuilder(option);
        }
        /// <summary>
        /// 获取TcpSocket服务端构建者
        /// </summary>
        /// <param name="port">监听端口</param>
        /// <returns></returns>
        public static ITcpSocketServerBuilder GetTcpSocketServerBuilder(int port)
        {
            var option = new ServerOption() { Port = port };
            return GetTcpSocketServerBuilder(option);
        }
        //------------------------------------------------------------------------------------------------
        /// <summary>
        /// 获取WebSocket服务端构建者
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static IWebSocketServerBuilder GetWebSocketServerBuilder(ServerOption option)
        {
            return new WebSocketServerBuilder(option);
        }
        /// <summary>
        /// 获取WebSocket服务端构建者
        /// </summary>
        /// <param name="port">监听端口</param>
        /// <param name="path">路径,默认为"/"</param>
        /// <returns></returns>
        public static IWebSocketServerBuilder GetWebSocketServerBuilder(int port, string path = "/")
        {
            var option = new ServerOption() { Port = port, Path = path };
            return GetWebSocketServerBuilder(option);
        }
        //------------------------------------------------------------------------------------------------
        /// <summary>
        /// 获取WebSocket客户端构建者
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static IWebSocketClientBuilder GetWebSocketClientBuilder(ClientOption option)
        {
            return new WebSocketClientBuilder(option);
        }
        /// <summary>
        /// 获取WebSocket客户端构建者
        /// </summary>
        /// <param name="ip">服务器Ip</param>
        /// <param name="port">服务器端口</param>
        /// <param name="path">路径,默认为"/"</param>
        /// <returns></returns>
        public static IWebSocketClientBuilder GetWebSocketClientBuilder(string ip, int port, string path = "/")
        {
            var option = new ClientOption() { SeverIp = ip, Port = port, Path = path };
            return GetWebSocketClientBuilder(option);
        }
        /// <summary>
        /// 获取WebSocket客户端构建者
        /// </summary>
        /// <param name="remoteAddress">服务器Ip 服务器端口</param>
        /// <param name="path">路径,默认为"/"</param>
        /// <returns></returns>
        public static IWebSocketClientBuilder GetWebSocketClientBuilder(EndPoint remoteAddress, string path = "/")
        {
            var eip = remoteAddress as System.Net.IPEndPoint;
            return GetWebSocketClientBuilder(eip.Address.ToString(), eip.Port, path);
        }
        //------------------------------------------------------------------------------------------------
        /// <summary>
        /// 获取Udpocket构建者
        /// 注:UDP服务端与客户端一样
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public static IUdpSocketBuilder GetUdpSocketBuilder(ClientOption option)
        {
            return new UdpSocketBuilder(option);
        }
        /// <summary>
        /// 获取Udpocket构建者
        /// 注:UDP服务端与客户端一样
        /// </summary>
        /// <param name="port">监听端口,作为客户端时可不设置</param>
        /// <returns></returns>
        public static IUdpSocketBuilder GetUdpSocketBuilder(int port = 0)
        {
            var option = new ClientOption() { Port = port };
            return GetUdpSocketBuilder(option);
        }
    }
}
