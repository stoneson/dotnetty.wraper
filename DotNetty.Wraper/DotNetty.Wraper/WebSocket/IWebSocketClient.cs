﻿namespace DotNetty.Wraper
{
    /// <summary>
    /// WebSocket客户端
    /// </summary>
    public interface IWebSocketClient : IBaseTcpSocketClient, ISendString
    {

    }
}