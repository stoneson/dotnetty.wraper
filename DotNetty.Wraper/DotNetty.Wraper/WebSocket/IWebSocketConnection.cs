﻿namespace DotNetty.Wraper
{
    /// <summary>
    /// WebSocket连接
    /// </summary>
    public interface IWebSocketConnection : IBaseSocketConnection, ISendString
    {
        
    }
}
