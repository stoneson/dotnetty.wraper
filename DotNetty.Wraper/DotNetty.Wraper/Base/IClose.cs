﻿namespace DotNetty.Wraper
{
    /// <summary>
    /// 关闭接口
    /// </summary>
    public interface IClose
    {
        /// <summary>
        /// 关闭前处理
        /// </summary>
        System.Action BeforeClose { get; set; }
        /// <summary>
        /// 关闭
        /// </summary>
        void Close();
        /// <summary>
        /// 关闭后处理
        /// </summary>
        System.Action AfterClose { get; set; }
    }
}