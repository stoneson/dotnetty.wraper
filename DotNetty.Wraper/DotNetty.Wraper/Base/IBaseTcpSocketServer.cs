﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    /// <summary>
    /// TcpSocketServer基接口
    /// </summary>
    /// <typeparam name="SocketConnection">The type of the ocket connection.</typeparam>
    /// <seealso cref="DotNetty.Wraper.IClose" />
    public interface IBaseTcpSocketServer<SocketConnection> : IClose
        where SocketConnection : IBaseSocketConnection
    {
        /// <summary>
        /// 监听端口
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        int Port { get; }

        /// <summary>
        /// 设置连接名
        /// </summary>
        /// <param name="theConnection">连接</param>
        /// <param name="oldConnectionName">原连接名</param>
        /// <param name="newConnectionName">新连接名</param>
        void SetConnectionName(SocketConnection theConnection, string oldConnectionName, string newConnectionName);

        /// <summary>
        /// 通过连接Id获取连接
        /// </summary>
        /// <param name="connectionId">连接Id</param>
        /// <returns></returns>
        SocketConnection GetConnectionById(string connectionId);

        /// <summary>
        /// 通过连接名获取连接Id
        /// </summary>
        /// <param name="connectionName">连接名</param>
        /// <returns></returns>
        SocketConnection GetConnectionByName(string connectionName);

        /// <summary>
        /// 删除连接
        /// </summary>
        /// <param name="theConnection">连接</param>
        void RemoveConnection(SocketConnection theConnection);

        /// <summary>
        /// 获取所有连接名
        /// </summary>
        /// <returns></returns>
        List<string> GetAllConnectionNames();

        /// <summary>
        /// 获取所有连接
        /// </summary>
        /// <returns></returns>
        List<SocketConnection> GetAllConnections();

        /// <summary>
        /// 获取当前连接数量
        /// </summary>
        /// <returns></returns>
        int GetConnectionCount();

        /// <summary>
        /// 发送字节
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <param name="clientid">目标地址</param>
        /// <returns></returns>
        Task Send(byte[] bytes, string clientid);

        /// <summary>
        /// 发送字符串,UTF-8编码
        /// </summary>
        /// <param name="msgStr">字符串</param>
        /// <param name="clientid">目标地址</param>
        /// <returns></returns>
        Task Send(string msgStr, string clientid);
    }
}