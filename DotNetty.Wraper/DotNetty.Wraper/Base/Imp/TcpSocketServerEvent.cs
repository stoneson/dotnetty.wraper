﻿using System;

namespace DotNetty.Wraper
{
    class TcpSocketServerEvent<TSocketServer, TConnection, TData>
    {
        public Action<TSocketServer,  Transport.Channels.IChannel> OnChannelRegistered { get; set; }
        public Action<TSocketServer, TConnection, Transport.Channels.IChannel> OnChannelUnregistered { get; set; }

        public Action<TSocketServer> OnServerStarted { get; set; }
        public Action<TSocketServer, TConnection> OnNewConnection { get; set; }
        public Action<TSocketServer, TConnection, TData> OnRecieve { get; set; }
        public Action<TSocketServer, TConnection, TData> OnSend { get; set; }
        public Action<TSocketServer, TConnection> OnConnectionClose { get; set; }
        public Action<Exception> OnException { get; set; }
    }
}
