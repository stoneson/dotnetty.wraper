﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetty.Wraper
{
    public class ClientOption
    {
        public string SeverIp { get; set; }
        public int Port { get; set; } = 0;
        public bool TcpNodelay { get; set; } = true;
        public bool SoKeepalive { get; set; } = true;
        public bool SoBroadcast { get; set; } = true;
        public int ConnectTimeout { get; set; } = 0;
        public bool UseLibuv { get; set; } = false;

        public string Certificate { get; set; }

        public string CertificatePassword { get; set; }

        /// <summary>
        /// WebSocket Path
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Shutdown with QuietPeriod MS
        /// </summary>
        public int QuietPeriod { get; set; } = 100;
        /// <summary>
        /// ShutdownTimeout MS
        /// </summary>
        public int ShutdownTimeout { get; set; } = 3;
    }
}
