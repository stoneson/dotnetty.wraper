﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetty.Wraper
{
    public class ServerOption
    {
        public AddressBindType BindType { get; set; } = AddressBindType.Any;

        public int Port { get; set; } = 7900;

        public int SoBacklog { get; set; } = 128;

        public int EventLoopCount { get; set; } = 1;

        public int QuietPeriod { get; set; } = 100;

        public int ShutdownTimeout { get; set; } = 3;

        /// <summary>
        /// 
        /// </summary>
        public bool UseLibuv { get; set; } = false;

        public string SpecialAddress { get; set; } = "127.0.0.1";

        public string StartupWords { get; set; } = "ServerHost bind at {0} \r\n";

        public string Certificate { get; set; }

        public string CertificatePassword { get; set; }

        /// <summary>
        /// WebSocket Path
        /// </summary>
        public string Path { get; set; }
    }

    public enum AddressBindType
    {
        Any, //0.0.0.0 Address.Any
        Loopback, // localhost
        InternalAddress, //本机内网地址
        SpecialAddress //自定义地址
    }
}
