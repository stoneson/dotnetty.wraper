using System;
using System.Diagnostics;
using DotNetty.Wraper;

namespace DotNetty.Wraper
{
#if !NET45
    public static class DiagnosticListenerExtensions
    {

        public const string DiagnosticListenerName = "DotNetty.WraperDiagnosticListener";

        public const string DiagnosticServiceReceive = "DotNetty.Wraper.Service.Receive";
        public const string DiagnosticServiceReceiveCompleted = "DotNetty.Wraper.Service.ReceiveCompleted";
        public const string DiagnosticServiceException = "DotNetty.Wraper.Service.Exception";

        public const string DiagnosticClientReceive = "DotNetty.Wraper.Client.Receive";
        public const string DiagnosticClientReceiveCompleted = "DotNetty.Wraper.Client.ReceiveCompleted";
        public const string DiagnosticClientException = "DotNetty.Wraper.Client.Exception";

        public static void ServiceReceive<TMessage>(this DiagnosticListener listener, TMessage ReceiveMessage)
        {
            if (listener.IsEnabled(DiagnosticServiceReceive))
            {
                listener.Write(DiagnosticServiceReceive, new {
                    Message = ReceiveMessage
                });
            }
        }

        public static void ServiceReceiveCompleted<TMessage>(this DiagnosticListener listener, TMessage ReceiveMessage)
        {
            if (listener.IsEnabled(DiagnosticServiceReceiveCompleted))
            {
                listener.Write(DiagnosticServiceReceiveCompleted, new {
                    Request = ReceiveMessage
                });
            }
        }

        public static void ServiceException(this DiagnosticListener listener, Exception exception)
        {
            if (listener.IsEnabled(DiagnosticServiceException))
            {
                listener.Write(DiagnosticServiceException, new {
                    Exception = exception
                });
            }
        }


        public static void ClientReceive<TMessage>(this DiagnosticListener listener, TMessage ReceiveMessage)
        {
            if (listener.IsEnabled(DiagnosticClientReceive))
            {
                listener.Write(DiagnosticClientReceive, new {
                    Message = ReceiveMessage
                });
            }
        }
        public static void ClientReceiveComplete<TMessage>(this DiagnosticListener listener, TMessage ReceiveMessage)
        {
            if (listener.IsEnabled(DiagnosticClientReceive))
            {
                listener.Write(DiagnosticClientReceive, new {
                    Message = ReceiveMessage
                });
            }
        }
        public static void ClientException(this DiagnosticListener listener, Exception exception)
        {
            if (listener.IsEnabled(DiagnosticClientException))
            {
                listener.Write(DiagnosticClientException, new {
                    Exception = exception
                });
            }
        }
    }
#endif
}
