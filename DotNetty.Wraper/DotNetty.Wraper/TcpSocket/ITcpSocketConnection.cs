﻿namespace DotNetty.Wraper
{
    /// <summary>
    /// TcpSocket连接
    /// </summary>
    public interface ITcpSocketConnection : IBaseSocketConnection, ISendString
    {

    }
}