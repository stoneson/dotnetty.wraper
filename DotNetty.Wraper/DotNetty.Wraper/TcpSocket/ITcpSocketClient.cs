﻿namespace DotNetty.Wraper
{
    /// <summary>
    /// TcpSocket客户端
    /// </summary>
    public interface ITcpSocketClient : IBaseTcpSocketClient, ISendBytes, ISendString
    {

    }
}