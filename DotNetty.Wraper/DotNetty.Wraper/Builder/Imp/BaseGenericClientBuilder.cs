﻿using System;

namespace DotNetty.Wraper
{
    abstract class BaseGenericClientBuilder<TBuilder, TTarget, TData> :
        BaseBuilder<TBuilder, TTarget, ClientOption ,BaseSimpleChannelInboundHandler<object>>,
        IGenericClientBuilder<TBuilder, TTarget, TData>
        where TBuilder : class
    {
        public BaseGenericClientBuilder(ClientOption _option)
            : base(_option)
        {
        }

        protected TcpSocketCientEvent<TTarget, TData> _event { get; }
            = new TcpSocketCientEvent<TTarget, TData>();

        public TBuilder OnClientClose(Action<TTarget> action)
        {
            _event.OnClientClose = action;

            return this as TBuilder;
        }

        public TBuilder OnClientStarted(Action<TTarget> action)
        {
            _event.OnClientStarted = action;

            return this as TBuilder;
        }

        public TBuilder OnRecieve(Action<TTarget, TData> action)
        {
            _event.OnRecieve = action;

            return this as TBuilder;
        }

        public TBuilder OnSend(Action<TTarget, TData> action)
        {
            _event.OnSend = action;

            return this as TBuilder;
        }

        public override TBuilder OnException(Action<Exception> action)
        {
            _event.OnException = action;

            return this as TBuilder;
        }

        public TBuilder OnChannelRegistered(Action<TTarget, Transport.Channels.IChannel> action)
        {
            _event.OnChannelRegistered = action;

            return this as TBuilder;
        }

        public TBuilder OnChannelUnregistered(Action<TTarget, Transport.Channels.IChannel> action)
        {
            _event.OnChannelUnregistered = action;

            return this as TBuilder;
        }
       
    }
}
