﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Bootstrapping;
using DotNetty.Transport.Channels;
using DotNetty.Transport.Channels.Sockets;
using System;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    class TcpSocketServerBuilder :
        BaseGenericServerBuilder<ITcpSocketServerBuilder, ITcpSocketServer, ITcpSocketConnection, byte[]>,
        ITcpSocketServerBuilder
    {
        public TcpSocketServerBuilder(ServerOption _option)
            : base(_option)
        {
            //this.AddLast(new Handlers.Logging.LoggingHandler());
        }

        //protected Action<IChannelPipeline> _setEncoder { get; set; }

        //public ITcpSocketServerBuilder SetLengthFieldDecoder(int maxFrameLength, int lengthFieldOffset, int lengthFieldLength, int lengthAdjustment, int initialBytesToStrip, ByteOrder byteOrder = ByteOrder.BigEndian)
        //{
        //    AddLast(new LengthFieldBasedFrameDecoder(byteOrder, maxFrameLength, lengthFieldOffset, lengthFieldLength, lengthAdjustment, initialBytesToStrip, true));

        //    return this;
        //}

        //public ITcpSocketServerBuilder SetLengthFieldEncoder(int lengthFieldLength)
        //{
        //    AddLast(new LengthFieldPrepender(lengthFieldLength));

        //    return this;
        //}
        ///// <summary>
        ///// Inserts multiple <see cref="IChannelHandler"/>s at the last position of this pipeline.
        ///// </summary>
        ///// <param name="handlers"></param>
        ///// <returns></returns>
        //public ITcpSocketServerBuilder AddLast(params IChannelHandler[] handlers)
        //{
        //    _setEncoder += x => x.AddLast(handlers);

        //    return this;
        //}
        ///// <summary>
        ///// Appends an <see cref="Transport.Channels.IChannelHandler"/> at the last position of this pipeline.
        ///// </summary>
        ///// <param name="name"></param>
        ///// <param name="handler"></param>
        ///// <returns></returns>
        //public ITcpSocketServerBuilder AddLast(string name, IChannelHandler handler)
        //{
        //    _setEncoder += x => x.AddLast(name, handler);

        //    return this;
        //}

        //public async override Task<ITcpSocketServer> BuildAsync(Func<ITcpSocketServer, IChannelHandler> addChannelHandler = null, Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null
        //     , bool useLibuv = false, System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate = null)
        //{
        //    return await BuildAsync(addChannelHandler, OnPipelineAction, useLibuv, tlsCertificate, 100, 1);
        //}

        public async override Task<ITcpSocketServer> BuildAsync(System.Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null, Func<ITcpSocketServer, BaseSimpleChannelInboundHandler<object>> addChannelHandler = null)
        {
            if (option == null) throw new ArgumentNullException("option");
            //Environment.SetEnvironmentVariable("io.netty.allocator.numDirectArenas", "0");
            //Environment.SetEnvironmentVariable("io.netty.allocator.numHeapArenas", "0");
            TcpSocketServer tcpServer = new TcpSocketServer(option.Port, _event);
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
            }
            System.Security.Cryptography.X509Certificates.X509Certificate2 tlsCertificate = null;
            if (!string.IsNullOrWhiteSpace(option.Certificate) && System.IO.File.Exists(option.Certificate))
            {
                tlsCertificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(option.Certificate, option.CertificatePassword);
            }
            IEventLoopGroup bossGroup;
            IEventLoopGroup workerGroup;//Default eventLoopCount is Environment.ProcessorCount * 2

            var bootstrap = new ServerBootstrap();
            if (option.UseLibuv)
            {
                var dispatcher = new Transport.Libuv.DispatcherEventLoopGroup();
                bossGroup = dispatcher;
                workerGroup = new Transport.Libuv.WorkerEventLoopGroup(dispatcher);

                bootstrap.Group(bossGroup, workerGroup);
                bootstrap.Channel<Transport.Libuv.TcpServerChannel>();

                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)
                       || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                {
                    bootstrap
                        .Option(ChannelOption.SoReuseport, true)
                        .ChildOption(ChannelOption.SoReuseaddr, true);
                }
            }
            else
            {
                bossGroup = new MultithreadEventLoopGroup(option.EventLoopCount);
                workerGroup = new MultithreadEventLoopGroup();

                bootstrap.Group(bossGroup, workerGroup);
                bootstrap.Channel<TcpServerSocketChannel>();
            }
            //var bootstrapChannel = await 
            bootstrap
            .Option(ChannelOption.SoBacklog, option.SoBacklog)
            .Handler(new Handlers.Logging.LoggingHandler("SRV-LSTN"))
            //.Handler(new Handlers.Logging.LoggingHandler("LSTN"))
            //.ChildOption(ChannelOption.Allocator, PooledByteBufferAllocator.Default)
            //.Group(bossGroup, workerGroup)
            .ChildHandler(new ActionChannelInitializer<IChannel>(channel =>
            {
                var pipeline = channel.Pipeline;
                if (tlsCertificate != null)
                {
                    pipeline.AddLast(Handlers.Tls.TlsHandler.Server(tlsCertificate));
                }
                pipeline.AddLast(new Handlers.Logging.LoggingHandler("SRV-CONN"));
                OnPipelineAction?.Invoke(pipeline);

                if (addChannelHandler != null)
                    pipeline.AddLast("tcpServer", addChannelHandler(tcpServer));
                else
                    pipeline.AddLast("tcpServer", new CommonChannelHandler(tcpServer));
            }));//.BindAsync(option.Port);
            IChannel _channel;
            if (option.BindType == AddressBindType.Any)
            {
                _channel = await bootstrap.BindAsync(option.Port);
            }
            else if (option.BindType == AddressBindType.InternalAddress)
            {
                var localPoint = IPUtility.GetLocalIntranetIP();
                if (localPoint == null)
                {
                    //this._logger.LogWarning("there isn't an avaliable internal ip address,the service will be hosted at loopback address.");
                    _channel = await bootstrap.BindAsync(System.Net.IPAddress.Loopback, option.Port);
                }
                else
                {
                    //this._logger.LogInformation("TcpServerHost bind at {0}",localPoint);
                    _channel = await bootstrap.BindAsync(localPoint, this.option.Port);

                }
            }
            else if (option.BindType == AddressBindType.Loopback)
            {
                _channel = await bootstrap.BindAsync(System.Net.IPAddress.Loopback, option.Port);
            }
            else
            {
                _channel = await bootstrap.BindAsync(System.Net.IPAddress.Parse(option.SpecialAddress), option.Port);
            }
            _event.OnServerStarted?.Invoke(tcpServer);

            tcpServer.SetChannel(_channel);
            tcpServer.AfterClose = () =>
            {
                Task.WhenAll(
                   bossGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(option.QuietPeriod), TimeSpan.FromSeconds(option.ShutdownTimeout)),
                   workerGroup.ShutdownGracefullyAsync(TimeSpan.FromMilliseconds(option.QuietPeriod), TimeSpan.FromSeconds(option.ShutdownTimeout))).Wait();
            };
            Console.Write(option.StartupWords, _channel.LocalAddress);
            return await Task.FromResult(tcpServer);
        }
    }
}