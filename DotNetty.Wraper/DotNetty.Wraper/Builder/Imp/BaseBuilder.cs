﻿using DotNetty.Transport.Channels;
using System;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    abstract class BaseBuilder<TBuilder, TTarget, TOption, TChannelHandler>
        : IBuilder<TBuilder, TTarget,TChannelHandler>
        where TBuilder : class
        where TChannelHandler : IChannelHandler
    {
        public BaseBuilder(TOption _option)
        {
            option = _option;
        }
        protected TOption option { get; }

        public abstract TBuilder OnException(Action<Exception> action);
        /// <summary>
        /// 构建目标生成类
        /// </summary>
        /// <param name="addChannelHandler"></param>
        /// <param name="OnPipelineAction"></param>
        /// <returns>TTarget</returns>
        public abstract Task<TTarget> BuildAsync(Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null,Func<TTarget, TChannelHandler> addChannelHandler = null);

    }
}
