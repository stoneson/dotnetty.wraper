﻿
using System;
using System.Threading.Tasks;

namespace DotNetty.Wraper
{
    /// <summary>
    /// 构建者
    /// </summary>
    /// <typeparam name="TBuilder">特定构建者</typeparam>
    /// <typeparam name="TTarget">目标生成类</typeparam>
    /// <typeparam name="TChannelHandler"></typeparam>
    public interface IBuilder<TBuilder, TTarget, TChannelHandler>
        where TChannelHandler : Transport.Channels.IChannelHandler
    {
        /// <summary>
        /// 异常处理
        /// </summary>
        /// <param name="action">异常处理委托</param>
        /// <returns></returns>
        TBuilder OnException(Action<Exception> action);

        /// <summary>
        /// 构建目标生成类
        /// </summary>
        /// <param name="OnPipelineAction"></param>
        /// <param name="addChannelHandler"></param>
        /// <returns>TTarget</returns>
        Task<TTarget> BuildAsync(Action<Transport.Channels.IChannelPipeline> OnPipelineAction = null,Func<TTarget, TChannelHandler> addChannelHandler = null);

    }
}
