#  DotNetty.Wraper

#### Description
将微软的Azure/DotNetty 代码重新整理,支持netstandard2.0;net472;net45，可以直接在各个 版本中使用。
并将多个类库合并成到一个项目中，加了些封装类，开箱即用。

封装类的示例：

Client
```cs
using DotNetty.Wraper;
using Examples.Common;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading.Tasks;

namespace TcpSocket.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();
            var option = new ClientOption();
            option.SeverIp = Examples.Common.ClientSettings.Host.ToString();
            option.Port = Examples.Common.ClientSettings.Port;
            option.UseLibuv = Examples.Common.ClientSettings.UseLibuv;
            if (Examples.Common.ServerSettings.IsSsl)
            {
                option.Certificate = System.IO.Path.Combine(ExampleHelper.ProcessDirectory, "dotnetty.com.pfx");
                option.CertificatePassword = "password";
            }
            var theClientBuild = SocketBuilderFactory.GetTcpSocketClientBuilder(option)
                .OnChannelRegistered((client, channel) =>
                {
                    _logger.LogDebug("OnChannelRegistered: " + channel.Id);
                })
                .OnChannelRegistered((client, channel) =>
                {
                    _logger.LogDebug("OnChannelRegistered: " + channel.Id);
                })
                .OnClientStarted(client =>
                {
                    _logger.LogInformation($"客户端启动");
                    client.Send("Hello world" + DateTime.Now);
                })
                .OnClientClose(client =>
                {
                    _logger.LogInformation($"客户端关闭");
                })
                .OnException(ex =>
                {
                    _logger.LogError($"异常:{ex.Message}");
                })
                .OnRecieve((client, bytes) =>
                {
                    _logger.LogInformation($"客户端:收到数据:{Encoding.UTF8.GetString(bytes)}");
                    client.Send("Hello world " + DateTime.Now);
                })
                .OnSend((client, bytes) =>
                {
                    _logger.LogInformation($"客户端:发送数据:{Encoding.UTF8.GetString(bytes)}");
                });

            var theClient = await theClientBuild .BuildAsync((pipeline) =>
            {
                pipeline.AddLast("framing-enc", new DotNetty.Codecs.LengthFieldPrepender(2));
                pipeline.AddLast("framing-dec", new DotNetty.Codecs.LengthFieldBasedFrameDecoder(ushort.MaxValue, 0, 2, 0, 2));
            });
            Console.ReadLine();

            theClient?.Close();
        }
    }
}
```
Server
```cs

using System;
using System.Text;
using System.Threading.Tasks;

using DotNetty.Wraper;
using Examples.Common;
using Microsoft.Extensions.Logging;

namespace TcpSocket.Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
            ILogger _logger = new CNative.Logging.ConfigLogger();

            var option = new ServerOption();
            option.Port = Examples.Common.ServerSettings.Port;
            option.UseLibuv = Examples.Common.ServerSettings.UseLibuv;
            if (Examples.Common.ServerSettings.IsSsl)
            {
                option.Certificate = System.IO.Path.Combine(ExampleHelper.ProcessDirectory, "dotnetty.com.pfx");
                option.CertificatePassword = "password";
            }
            var theServerBuild = SocketBuilderFactory.GetTcpSocketServerBuilder(option)
             .OnConnectionClose((server, connection) =>
             {
                 _logger.LogInformation($"连接关闭,连接名[{connection.ConnectionName}],当前连接数:{server.GetConnectionCount()}");
             })
             .OnException(ex =>
             {
                 _logger.LogError($"服务端异常:{ex.Message}");
             })
             .OnChannelRegistered((server, channel) =>
             {
                 _logger.LogDebug("ChannelUnregistered: " + channel.Id);
             })
             .OnChannelRegistered((server, channel) =>
             {
                 _logger.LogDebug("ChannelRegistered: " + channel.Id);
             })
             .OnNewConnection((server, connection) =>
             {
                 connection.ConnectionName = $"名字{connection.ConnectionId}";
                 _logger.LogInformation($"新的连接[{connection.ClientAddress.Address.MapToIPv4().ToString()}]:{connection.ConnectionName},当前连接数:{server.GetConnectionCount()}");
             })
             .OnRecieve((server, connection, bytes) =>
             {
                 _logger.LogInformation($"服务端:数据{Encoding.UTF8.GetString(bytes)}");
                 connection.Send(bytes);
             })
             .OnSend((server, connection, bytes) =>
             {
                 _logger.LogInformation($"向连接名[{connection.ConnectionName}]发送数据:{Encoding.UTF8.GetString(bytes)}");
             })
             .OnServerStarted(server =>
             {
                 _logger.LogInformation($"服务启动");
             });
            var theServer = await theServerBuild.BuildAsync((pipeline) =>
            {
                pipeline.AddLast("framing-enc", new DotNetty.Codecs.LengthFieldPrepender(2));
                pipeline.AddLast("framing-dec", new DotNetty.Codecs.LengthFieldBasedFrameDecoder(ushort.MaxValue, 0, 2, 0, 2));
            });

            Console.ReadLine();

            theServer?.Close();
        }
    }
}


```
# DotNetty Project

[![Join the chat at https://gitter.im/Azure/DotNetty](https://img.shields.io/gitter/room/Azure/DotNetty.js.svg?style=flat-square)](https://gitter.im/Azure/DotNetty?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![Available on NuGet https://www.nuget.org/packages?q=DotNetty.Wraper](https://img.shields.io/nuget/v/DotNetty.Wraper.svg?style=flat-square)](https://www.nuget.org/packages?q=DotNetty.Wraper)
[![AppVeyor](https://img.shields.io/appveyor/ci/nayato/dotnetty.svg?label=appveyor&style=flat-square)](https://ci.appveyor.com/project/nayato/dotnetty)

DotNetty.Wraper is a port of [Netty](https://github.com/netty/netty), asynchronous event-driven network application framework for rapid development of maintainable high performance protocol servers & clients.

## Use

* Official releases are on [NuGet](https://www.nuget.org/packages?q=DotNetty.Wraper).
* Nightly builds are available on [MyGet](https://www.myget.org/F/dotnetty/api/v2/).

## Contribute

We gladly accept community contributions.

* Issues: Please report bugs using the Issues section of GitHub
* Source Code Contributions:
 * Please follow the [Contribution Guidelines for Microsoft Azure](http://azure.github.io/guidelines.html) open source that details information on onboarding as a contributor
 * See [C# Coding Style](https://github.com/Azure/DotNetty/wiki/C%23-Coding-Style) for reference on coding style.

