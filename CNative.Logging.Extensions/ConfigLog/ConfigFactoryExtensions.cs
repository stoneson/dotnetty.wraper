﻿using CNative.Logging.Nlog;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging
{
    public static class ConfigFactoryExtensions
    {
        public static ILoggerFactory AddConfigLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new ConfigLoggerProvider());
            return factory;
        }

#if NETSTANDARD2_1

        public static ILoggingBuilder AddConfigLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(new ConfigLoggerProvider());
            return builder;
        }
#endif
    }
}
