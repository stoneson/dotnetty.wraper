﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging
{
    public class ConfigLoggerProvider : ILoggerProvider
    {
        /// <summary>
        /// 默认构造函数，根据Provider进此构造函数
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new ConfigLogger();
        }

        public void Dispose()
        {
        }
    }
}
