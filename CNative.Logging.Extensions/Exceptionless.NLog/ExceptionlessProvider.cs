﻿#if NETSTANDARD2_1
using CNative.Exceptionless.NLog;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging.Exceptionless.NLog
{
    public class ExceptionlessProvider : ILoggerProvider
    {
        /// <summary>
        /// 默认构造函数，根据Provider进此构造函数
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new ExceptionlessLogger(categoryName);
        }

        public void Dispose()
        {
        }
    }
}
#endif