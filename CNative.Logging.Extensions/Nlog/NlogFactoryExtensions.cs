﻿using CNative.Logging.Nlog;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging
{
    public static class NlogFactoryExtensions
    {
        public static ILoggerFactory AddNlogFileLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new FileLoggerProvider());
            return factory;
        }

#if NETSTANDARD2_1

        public static ILoggingBuilder AddNlogFileLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(new FileLoggerProvider());
            return builder;
        }
#endif
    }
}
