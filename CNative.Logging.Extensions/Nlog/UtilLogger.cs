﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging.Nlog
{
    public class UtilLogger<T>
    {
        private static ILogger iLog;
        public static ILogger Log
        {
            get
            {
                if (iLog != null) return iLog;

                ////第一种写法
                //ILoggerFactory loggerFactory = new LoggerFactory();
                //loggerFactory.AddFileLogger();
                //iLog = loggerFactory.CreateLogger<DbCommand>();

                //第二种写法
                iLog = new LoggerFactory().AddNlogFileLogger().CreateLogger<T>();
                return iLog;
            }
            set => iLog = value;
        }
    }
}
