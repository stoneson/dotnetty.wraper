﻿using CNative.Logging.Nlog;
using CNative.Logging.Serilog;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace CNative.Logging
{
    public static class SerilogFactoryExtensions
    {
        public static ILoggerFactory AddSerilogLogger(this ILoggerFactory factory)
        {
            factory.AddProvider(new SerilogLoggerProvider());
            return factory;
        }

#if NETSTANDARD2_1

        public static ILoggingBuilder AddSerilogLogger(this ILoggingBuilder builder)
        {
            builder.AddProvider(new SerilogLoggerProvider());
            return builder;
        }
#endif
    }
}
